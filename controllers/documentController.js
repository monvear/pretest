const Document = require('../models/documentModel')
const redis = require('ioredis').createClient();

exports.setDocument = async (req,res) => {
    let {
        id,
        name,
        type,
        folder_id,
        content,
        timestamp,
        owner_id,
        share,
        company_id,
    } = req.body;
    let data;
    try {
        const checker = await Document.findOne({ id });
        if (checker) {
            const document = {
                id,
                name,
                type,
                folder_id,
                content,
                timestamp,
                owner_id,
                share,
                company_id,
            };
             data = await Document.updateOne(
                {
                    id,
                },
                {
                    ...document,
                },
            );
        } else {
            const newDocument = new Document({
                id,
                name,
                type,
                folder_id,
                content,
                timestamp,
                owner_id,
                share,
                company_id,
            });
            
            data = await Document.create(newDocument);

            const newDocumentData = await Document.find();
            await redis.set('documentData', JSON.stringify(newDocumentData));
        }
        res.status(200).json({
            error: false,
            message: 'success set document!!!',
            data
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error,
        });
    }
}


exports.getListFolder = async (req,res) => {
    const  folder_id  = req.params.folder_id;
		let data = [];
		// console.log('from cache');
		try {
			const cachedListFilePerFolderData = await redis.get('listFilePerFolder');
			if (cachedListFilePerFolderData) {
				const listFilePerFolder = JSON.parse(cachedListFilePerFolderData);
				if (listFilePerFolder.parameter_folderId === folder_id) {
					data = listFilePerFolder.data;
				} else {
					data = await Document.find({ folder_id });
					await redis.set(
						'listFilePerFolder',
						JSON.stringify({ data, parameter_folderId: folder_id }),
					);
				}
			} else {
				data = await Document.find({ folder_id });
				await redis.set(
					'listFilePerFolder',
					JSON.stringify({ data, parameter_folderId: folder_id }),
				);
			}
			res.status(200).json({
				error: false,
				data,
			});
		} catch (error) {
			res.status(500).json({
				error,
			});
		}
}


exports.getDetail = async (req,res) => {
    const document_id = req.params.document_id;
    try {
        const cachedGetDetailDocumentData = await redis.get('getDetailDocument');
        if (cachedGetDetailDocumentData) {
            let getDetailDocumentData = JSON.parse(cachedGetDetailDocumentData);
            if (getDetailDocumentData.id == document_id) {
                res.status(200).json({
                    error: false,
                    data: getDetailDocumentData,
                });
            } else {
                const data = await Document.findOne({ id: document_id });
                await redis.set('getDetailDocument', JSON.stringify(data));
                res.status(200).json({
                    error: false,
                    data,
                });
            }
        } else {
            const data = await Document.findOne({ id: document_id });
            await redis.set('getDetailDocument', JSON.stringify(data));
            res.status(200).json({
                error: false,
                data,
            });
        }
    } catch (error) {
        res.status(500).json({
            error,
        });
    }
}

exports.deleteDocument = async (req,res) => {
    try {
        let id = req.body.id
        await Document.findOneAndDelete({id});
        const newDocumentData = await Document.find();
        await redis.set('documentData', JSON.stringify(newDocumentData));
        res.status(200).json({
            error: false,
            message: 'Success delete document!!!',
        });
    } catch (err) {
        console.log(err)
			res.status(500).json({
				err,
			});
    }
}
