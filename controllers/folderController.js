const Folder = require('../models/folderModel')
const redis = require('ioredis').createClient();

exports.setFolder = async (req,res) => {
    let { id, name, timestamp } = req.body;
		let user_id = req.user_id;
		let company_id = req.company_id;
		try {
			const checker = await Folder.findOne({ id });
			if (checker) {
				const folder = {
					id,
					name,
					timestamp,
					owner_id: user_id,
					company_id,
				};
				const data = await Folder.updateOne(
					{
						id,
					},
					{
						...folder,
					},
				);
				res.status(200).json({
					error: false,
					message: 'folder updated',
					data,
				});
			} else {
				const newfolder = new Folder({
					id,
					name,
					timestamp,
					owner_id: user_id,
					company_id,
				});
				const data = await Folder.create(newfolder);
				const newFolderData = await Folder.find();
				await redis.set('folderData', JSON.stringify(newFolderData));
				res.status(200).json({
					error: false,
					message: 'Folder created!!',
					data,
				});
			}
		} catch (err) {
            // console.log(err)
			res.status(500).json({
				err,
			});
		}
}

exports.deleteFolder = async (req,res) => {
    try {
        let id = req.body.id
        await Folder.findOneAndDelete({id});
        const newFolderData = await Folder.find();
        await redis.set('folderData', JSON.stringify(newFolderData));
        res.status(200).json({
            error: false,
            message: 'Success delete folder',
        });
    } catch (err) {
        // console.log(err)
			res.status(500).json({
				err,
			});
    }
}

