const Folder = require('../models/folderModel')
const Document = require('../models/documentModel')
const redis = require('ioredis').createClient();


exports.getAll = async (req,res) => {
// console.log(redis)
    try {
        const cachedFolderData = await redis.get('folderData');
        const cachedDocumentData = await redis.get('documentData');
        let folderData = [];
        let documentData = [];
        switch (true) {
            case cachedFolderData && cachedDocumentData:
                folderData = JSON.parse(cachedFolderData);
                documentData = JSON.parse(cachedDocumentData);
                res.status(200).json({
                    error: false,
                    data: [...folderData, ...documentData],
                });
                break;
            case cachedFolderData && !cachedDocumentData:
                folderData = JSON.parse(cachedFolderData);
                documentData = await Document.find();
                await redis.set('documentData', JSON.stringify(documentData));
                res.status(200).json({
                    error: false,
                    data: [...folderData, ...documentData],
                });
                break;
            case cachedDocumentData && !cachedFolderData:
                documentData = JSON.parse(cachedDocumentData);
                folderData = await Folder.find();
                await redis.set('folderData', JSON.stringify(folderData));
                res.status(200).json({
                    error: false,
                    data: [...folderData, ...documentData],
                });
                break;
            default:
                documentData = await Document.find();
                await redis.set('documentData', JSON.stringify(documentData));
                folderData = await Folder.find();
                await redis.set('folderData', JSON.stringify(folderData));
                res.status(200).json({
                    error: false,
                    data: [...folderData, ...documentData],
                });
                break;
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({ error });
    }
}
