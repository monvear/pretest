const mongoose = require('mongoose')
const MONGO_DB = 'mongodb://localhost/document-serivices'

// Connect to Mongoose and set connection variable
mongoose.connect(MONGO_DB, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})

mongoose.Promise = global.Promise
mongoose.pluralize(null)

const config = {
  JWT_SECRET: 'H1P0SECR37K3Y',
  JWT_EXPIRED: 60 * 10,
  JWT_REFRESH_EXPIRED: 60 * 60 * 24 * 30
}

module.exports = {
  common: config,
  db: mongoose
}