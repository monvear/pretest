var mongoose = require('mongoose');


var DocumentSchema = new mongoose.Schema({
    id: {
        type: String
    },
    name: {
        type: String
    },
    type: {
        type: String
    },
    folder_id: {
        type: String
    },
    content: {
        type: Object,
    },
    timestamps: {
        type: Number
    },
    owner_id: {
        type: String
    },
    share: {
        type: Array
    },
    company_id: {
        type: String
    }
})


const Document = module.exports = mongoose.model('document', DocumentSchema);

// Get Callback methods
module.exports.get = function (callback, limit) {
    Document.find(callback).limit(limit);
}