var mongoose = require('mongoose');


var FolderSchema = new mongoose.Schema({
    id: {
        type: String
    },
    name: {
        type: String
    },
    type: {
        type: String,
        default: `folder`
    },
    timestamps: {
        type: String
    },
    owner_id: {
        type: String
    },
    company_id: {
        type: String
    },
    content: {
        type: Object
    }
})


const Folder = module.exports = mongoose.model('folder', FolderSchema);

// Get Callback methods
module.exports.get = function (callback, limit) {
    Folder.find(callback).limit(limit);
}
