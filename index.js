// Import modules
require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
// const redis = require("redis")
const app = express()
const cp = require('cookie-parser')
const cors = require('cors')

// Import routes
const apiRoutes = require("./routes/api")

// configuration
const {common, db} = require('./config') 

const redis = require('redis')
 
// create and connect redis client to local instance.
const client = redis.createClient(6379)
 
// echo redis errors to the console
client.on('error', (err) => {
    console.log("Error " + err)
});

//remove x-powered-by
app.disable('x-powered-by')

// redis connect 
// const rediscl = redis.createClient()
// rediscl.on("connect", function () {
//     console.log("Redis plugged in.")
// })

//Cookie-parser
app.use(cp())

// cors
app.use(cors())

// set logger api
// app.use(logger('dev'))
// app.use(morgan('combined'))

// jwt secret token
app.set('secretKey', common.JWT_SECRET)

//static asset
app.use(express.static('public'))

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())


db.connection.on('error', console.error.bind(console, 'MongoDB connection error:'))

// Setup server port
const port = process.env.PORT || 8100

// Use Api routes in the App
app.use(apiRoutes)

// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function(req, res, next) {
    let err = new Error('Not Found')
       err.status = 404
       next(err)
})

app.use(function (req, res, next) {
    res.header('Content-Type', 'application/json');
    next();
})

// handle errors
app.use(function(err, req, res, next) {
console.log(err)
    if(err.status === 404)
    res.status(404).json({message: "Not found"})
    else 
    res.status(500).json({message: "Something looks wrong :( !!!"})
})

// Launch app to listen to specified port
app.listen(port, function () {
    console.log("environment:" + process.env.NODE_ENV)
    console.log("Running Document Service on port " + port)
})
