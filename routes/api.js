// Initialize express router
let router = require('express').Router()

const route = require('color-convert/route');
// Require controller
const rootController = require('../controllers/rootController');
const folderController = require('../controllers/folderController');
const documentController = require('../controllers/documentController');

// middleware
const auth = require('../middleware/auth')

// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to Document-services crafted with love!',
    })
})

// getAll document and folder 
router.route('/document-service')
.get([auth.verifyToken],rootController.getAll)

//folderController
router.route('/document-service/folder')
.post([auth.verifyToken],folderController.setFolder)
.delete([auth.verifyToken],folderController.deleteFolder)

//DocumentController
router.route('/document-service/folder/:folder_id')
.get([auth.verifyToken],documentController.getListFolder)

router.route('/document-service/document')
.post([auth.verifyToken],documentController.setDocument)
.delete([auth.verifyToken],documentController.deleteDocument)

router.route('/document-service/document/:document_id')
.get([auth.verifyToken],documentController.getDetail)




// Export API routes
module.exports = router
