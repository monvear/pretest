let jwt = require('jsonwebtoken')

verifyToken = (req, res, next) => {
  function verifyToken(token){
    return jwt.verify(token, process.env.JWT_SECRET)
  } 
  let { authorization } = req.headers;
	try {
		let decoded = verifyToken(authorization.split(' ')[1]);
		if (decoded) {
			let { user_id, company_id } = decoded;
			req.user_id = user_id;
			req.company_id = company_id;
			next();
		} else {
			res.status(401).json({
				message: 'not authorized',
			});
		}
	} catch (error) {
		console.log(error);
		res.status(500).json({
			message: error.message,
		});
	}
};

const authJwt = {
  verifyToken
};
module.exports = authJwt;