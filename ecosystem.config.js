module.exports = {
  apps : [{
    name: "document-services",
    script: "./index.js",
    env: {
      NODE_ENV: "development",
    }
  }]
};
